package models

import (
	"testing"
	"time"
)

func TestTemp(t *testing.T) {
	got := Temp{Temp: "28 °C", Time: time.Now()}
	if got.Temp != "28 °C" {
		t.Errorf("got.Temp = '%s'; want '28 °C'", got.Temp)
	}
}

func TestUpdate(t *testing.T) {
	var got Temp
	got.UpdateTemp()
	if got.Temp != "29 °C" {
		t.Errorf("got.Temp = '%s'; want '28 °C'", got.Temp)
	}
}

func ExampleTemp_UpdateTemp() {
	temp := Temp{}
	temp.UpdateTemp()
}
