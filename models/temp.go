// Package models provides strucs and functions to manage data temperature.
package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

// Struct representing a temperature.
type Temp struct {
	Time time.Time `json:"time"`
	Temp string    `json:"temp"`
}

// Function to update current temperature in Paris
func (t *Temp) UpdateTemp() {
	response, err := http.Get("https://api.openweathermap.org/data/2.5/weather?id=2988507&units=metric&appid=0a0586c30928c2f2c9711cedfeda8ed4")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		var result map[string]interface{}
		json.Unmarshal([]byte(data), &result)
		main := result["main"].(map[string]interface{})
		(*t).Temp = strconv.FormatFloat(main["temp"].(float64), 'f', 1, 64) + " °C"
		(*t).Time = time.Now()
	}
}
