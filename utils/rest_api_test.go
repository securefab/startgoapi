package utils

import (
	"github.com/gorilla/mux"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// Tests if all HTTP Security Headers are added as expected
func TestHTTPResponseMiddleware(t *testing.T) {
	r, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	handler := HTTPResponseMiddleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
	handler.ServeHTTP(w, r)

	resp := w.Result()
	for header, value := range ApiHttpHeaders {
		if val, ok := resp.Header[header]; ok {
			if val[0] != strings.Join(value[:], " ") {
				t.Errorf("Got %s expected %s", val[0], strings.Join(value[:], " "))
			}
		} else {
			t.Errorf(`Missing %s: "%s" HTTP Header`, header, strings.Join(value[:], " "))
		}
	}
}

// Example to use the middleware with mux.Router.
func ExampleHTTPResponseMiddleware() {
	router := mux.NewRouter()
	router.Use(HTTPResponseMiddleware)

	http.ListenAndServeTLS(":8000", "ssl/server.cert", "ssl/server.key", router)
}
