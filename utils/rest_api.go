// Package utils provides usefull function to create secure rest API.
package utils

import (
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strings"
)

// HTTP security headers recommanded by OWASP:
// https://cheatsheetseries.owasp.org/cheatsheets/REST_Security_Cheat_Sheet.html
var ApiHTTPHeaders http.Header = http.Header{
	"Content-Type":           {"application/json"},
	"X-Content-Type-Options": {"nosniff"},
	"X-Frame-Option":         {"deny"},
}

// HTTPResponseMiddleware is a mux.Router middleware adding security headers to HTTP responses.
//
// The middleware logs HTTP request. Log example :
//  {"URI":"/","host":"localhost:4443","level":"info","method":"GET","msg":"Access to URL","protcol":"HTTP/2.0","remoteAddr":"127.0.0.1:50328","time":"2019-08-13T20:35:43+02:00"}
func HTTPResponseMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"protcol": r.Proto,
			"remoteAddr": r.RemoteAddr,
			"method": r.Method,
			"host": r.Host,
			"URI": r.RequestURI,
		}).Info("Access to URL")
		for header, value := range ApiHTTPHeaders {
			w.Header().Set(header, strings.Join(value[:], " "))
		}
		next.ServeHTTP(w, r)
	})
}

// ConfigureLogger use github.com/sirupsen/logrus to output logs to standard output in Json format.
// Example :
//  {"level":"info","msg":"Starting API server on 0.0.0.0:4443","time":"2019-08-13T20:34:44+02:00"}
func ConfigureLogger() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
}
