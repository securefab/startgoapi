// startgoapi provides an example of secure REST API.
//
// This example can be called for getting temperature in Paris.
package main

import (
	"flag"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
	"gitlab.com/securefab/startgoapi/utils"
)

func createRouter() *mux.Router {
	router := mux.NewRouter()
	router.Use(utils.HTTPResponseMiddleware)
	router.HandleFunc("/", GetRoot).Methods("GET")
	router.HandleFunc("/test", GetTest).Methods("GET")
	return router
}

func StartAPIServer(addr string) {
	log.Fatal(http.ListenAndServeTLS(addr, "ssl/server.cert", "ssl/server.key", createRouter()))
}

func main() {
	utils.ConfigureLogger()
	addr := flag.String("a", "0.0.0.0:4443", "address to bind")
	log.Println("Starting API server on", *addr)
	StartAPIServer(*addr)
}
