package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"gitlab.com/securefab/startgoapi/models"
)

type Test struct {
	Value string `json:"value"`
}

// Handle GET / request
func GetRoot(w http.ResponseWriter, r *http.Request) {
	var temp models.Temp
	temp.UpdateTemp()
	fmt.Printf("Temperature is : %s", temp.Temp)
	json.NewEncoder(w).Encode(temp)
}

// Handle GET /test request
func GetTest(w http.ResponseWriter, r *http.Request) {
	var test Test
	test.Value = "Valeur de test"
	json.NewEncoder(w).Encode(test)
}
