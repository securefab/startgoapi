FROM golang:alpine AS builder

ENV BIN_PATH="/go/bin/app"
ENV PACKAGE_NAME="startgoapi"

# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git upx curl
WORKDIR $GOPATH/src/$PACKAGE_NAME

# Create appuser.
RUN adduser -D -g '' appuser

COPY . .

# Fetch dependencies.
RUN go get -d -v

# Build, optimize and pack the binary.
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o $BIN_PATH
RUN upx --brute $BIN_PATH

############################
# Create a small and secure image
############################
FROM scratch

MAINTAINER securefab

# Copy /etc/passwd with appuser
COPY --from=builder /etc/passwd /etc/passwd

# Copy app binary and dependencies (found with ldd)
COPY --from=builder /go/bin/app /app
COPY --from=builder /go/src/startgoapi/ssl ssl
COPY --from=builder /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1

# Copy curl binary and associated libraries (found with ldd)
COPY --from=builder /usr/bin/curl /curl
COPY --from=builder /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1
COPY --from=builder /usr/lib/libcurl.so.4 /usr/lib/libcurl.so.4
COPY --from=builder /lib/libz.so.1 /lib/libz.so.1
COPY --from=builder /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1
COPY --from=builder /usr/lib/libnghttp2.so.14 /usr/lib/libnghttp2.so.14
COPY --from=builder /lib/libssl.so.1.1 /lib/libssl.so.1.1
COPY --from=builder /lib/libcrypto.so.1.1 /lib/libcrypto.so.1.1

# Copy CA certificates (used by https requests)
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

USER appuser

EXPOSE 4443

HEALTHCHECK --timeout=3s CMD curl -f http://localhost/ || exit 1

ENTRYPOINT ["/app"]